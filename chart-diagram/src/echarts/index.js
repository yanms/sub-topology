import { s8, createDiv, rectangle } from 'dcim-topology/core';
export var echartsData = {};
export function echarts(ctx, node) {

    // console.log('echarts绘制-----node', node);
    
    // 绘制一个底图，类似于占位符。
    rectangle(ctx, node);
    // tslint:disable-next-line:no-shadowed-variable
    var echarts = echartsData.echarts || window.echarts;
    
    if (!node.data || !echarts) {
        return;
    }
    if (typeof node.data === 'string') {
        node.data = JSON.parse(node.data);
    }
    if (!node.data.echarts) {
        return;
    }
    if (!node.elementId) {
        node.elementId = s8();
    }
    if (!node.elementLoaded) {
        echartsData[node.id] = {
            div: createDiv(node)
        };
        node.elementLoaded = true;
        document.body.appendChild(echartsData[node.id].div);
        // 添加当前节点到div层
        node.addToDiv();
        echartsData[node.id].chart = echarts.init(echartsData[node.id].div, node.data.echarts.theme);
        node.elementRendered = false;
        // 等待父div先渲染完成，避免初始图表控件太大
        setTimeout(function () {
            echartsData[node.id].chart.resize();
        });
    }
    if (!node.elementRendered) {
        // 初始化时，等待父div先渲染完成，避免初始图表控件太大。
        setTimeout(function () {

            let option = {...node.data.echarts.option};
            if(node.appearance.type === 'line' || node.appearance.type === 'bar') {
                option.color = node.appearance.color;
                option.xAxis[0].axisLabel.formatter = function(value, index){
                    if (index === 0 || index%(node.appearance.intervalNumX) === 0){
                          return value;
                      }
                      else {
                          return '';
                      }
                    }

                option.yAxis[0].axisLabel.formatter = function(value, index){
                    if (index === 0 || index%(node.appearance.intervalNumY) === 0){
                        return value;
                    }
                    else {
                        return '';
                    }
                }
                // if(option.series.length >= 2) {

                //     for(let i=0; i<option.series.length-1 ; i++){

                //         option.color.push(node.appearance.color[0])
                //     }
                // }
                option.yAxis[0].splitLine = node.appearance.splitLine; 
                
            }else if(node.appearance.type === 'gauge') {
                option.series[0].axisLine.lineStyle = node.appearance.axisLine.lineStyle;
                option.series[0].startAngle = node.appearance.startAngle;
                option.series[0].endAngle = node.appearance.endAngle;
                option.series[0].min = node.appearance.min;
                option.series[0].max = node.appearance.max;
                option.series[0].splitNumber = node.appearance.splitNumber;
                option.series[0].axisTick = node.appearance.axisTick;
                option.series[0].splitLine = node.appearance.splitLine;
                option.series[0].axisLabel = node.appearance.axisLabel;
                option.series[0].pointer = node.appearance.pointer;
                option.series[0].itemStyle = node.appearance.itemStyle;
                option.series[0].detail.fontSize = node.appearance.detail.fontSize;
                option.series[0].detail.color = node.appearance.detail.color;
            }

            if(node.appearance.type === 'line') {

                option.series[0].lineStyle = node.appearance.lineStyle;

            }
            option.title = node.appearance.title;
            option.backgroundColor = node.appearance.backgroundColor;            
            echartsData[node.id].chart.setOption(option);
            echartsData[node.id].chart.resize();
            node.elementRendered = true;
        });
    }
}
//# sourceMappingURL=index.js.map