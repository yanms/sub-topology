import { registerNode, loadJS } from 'dcim-topology/core';
import { echarts, echartsData } from './echarts';
import './echarts/echarts.min.js';
export function register(_echarts) {
    echartsData.echarts = _echarts;
    if (process.browser && !echartsData.echarts && !window.echarts) {
        loadJS('https://cdn.bootcdn.net/ajax/libs/echarts/4.8.0/echarts.min.js', null, true);
    }
    registerNode('echarts', echarts, null, null, null);
}
//# sourceMappingURL=register.js.map