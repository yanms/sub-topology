import { Node } from 'dcim-topology/core';
export declare function simpleClass(ctx: CanvasRenderingContext2D, node: Node): void;
export declare function interfaceClass(ctx: CanvasRenderingContext2D, node: Node): void;
