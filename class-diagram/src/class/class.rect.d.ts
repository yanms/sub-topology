import { Node } from 'dcim-topology/core';
export declare function simpleClassIconRect(node: Node): void;
export declare function simpleClassTextRect(node: Node): void;
export declare function interfaceClassIconRect(node: Node): void;
export declare function interfaceClassTextRect(node: Node): void;
