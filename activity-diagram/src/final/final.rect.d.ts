import { Node } from 'dcim-topology/core';
export declare function activityFinalIconRect(node: Node): void;
export declare function activityFinalTextRect(node: Node): void;
