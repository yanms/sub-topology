import { Node } from 'dcim-topology/core';
export declare function swimlaneVIconRect(node: Node): void;
export declare function swimlaneVTextRect(node: Node): void;
