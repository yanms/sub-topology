import { Node } from 'dcim-topology/core';
export declare function fork(ctx: CanvasRenderingContext2D, node: Node): void;
