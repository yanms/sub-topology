import { Node } from 'dcim-topology/core';
export declare function forkIconRect(node: Node): void;
export declare function forkTextRect(node: Node): void;
