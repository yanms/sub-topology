import { Node } from 'dcim-topology/core';
export declare function forkHAnchors(node: Node): void;
export declare function forkVAnchors(node: Node): void;
