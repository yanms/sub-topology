import { Node } from 'dcim-topology/core';
export declare function swimlaneHIconRect(node: Node): void;
export declare function swimlaneHTextRect(node: Node): void;
