import { Node } from 'dcim-topology/core';
export declare function sequenceFocus(ctx: CanvasRenderingContext2D, node: Node): void;
