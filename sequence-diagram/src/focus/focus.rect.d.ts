import { Node } from 'dcim-topology/core';
export declare function sequenceFocusIconRect(node: Node): void;
export declare function sequenceFocusTextRect(node: Node): void;
