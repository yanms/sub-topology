import { Node } from 'dcim-topology/core';
export declare function lifelineIconRect(node: Node): void;
export declare function lifelineTextRect(node: Node): void;
