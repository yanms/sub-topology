import { Node } from 'dcim-topology/core';
export declare function lifeline(ctx: CanvasRenderingContext2D, node: Node): void;
