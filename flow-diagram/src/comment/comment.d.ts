import { Node } from 'dcim-topology/core';
export declare function flowComment(ctx: CanvasRenderingContext2D, node: Node): void;
