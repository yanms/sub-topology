import { Node } from 'dcim-topology/core';
export declare function flowSubprocessIconRect(node: Node): void;
export declare function flowSubprocessTextRect(node: Node): void;
