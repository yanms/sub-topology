import { Node } from 'dcim-topology/core';
export declare function flowDisplay(ctx: CanvasRenderingContext2D, node: Node): void;
