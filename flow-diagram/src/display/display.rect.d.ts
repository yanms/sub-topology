import { Node } from 'dcim-topology/core';
export declare function flowDisplayIconRect(node: Node): void;
export declare function flowDisplayTextRect(node: Node): void;
