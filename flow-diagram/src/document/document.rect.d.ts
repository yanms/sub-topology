import { Node } from 'dcim-topology/core';
export declare function flowDocumentIconRect(node: Node): void;
export declare function flowDocumentTextRect(node: Node): void;
