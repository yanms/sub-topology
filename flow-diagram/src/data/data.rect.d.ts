import { Node } from 'dcim-topology/core';
export declare function flowDataIconRect(node: Node): void;
export declare function flowDataTextRect(node: Node): void;
