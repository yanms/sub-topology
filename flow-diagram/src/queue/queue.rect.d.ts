import { Node } from 'dcim-topology/core';
export declare function flowQueueIconRect(node: Node): void;
export declare function flowQueueTextRect(node: Node): void;
