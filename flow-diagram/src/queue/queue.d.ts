import { Node } from 'dcim-topology/core';
export declare function flowQueue(ctx: CanvasRenderingContext2D, node: Node): void;
