import { Node } from 'dcim-topology/core';
export declare function flowManuallyIconRect(node: Node): void;
export declare function flowManuallyTextRect(node: Node): void;
