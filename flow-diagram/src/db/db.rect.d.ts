import { Node } from 'dcim-topology/core';
export declare function flowDbIconRect(node: Node): void;
export declare function flowDbTextRect(node: Node): void;
