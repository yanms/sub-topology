import { Node } from 'dcim-topology/core';
export declare function flowInternalStorage(ctx: CanvasRenderingContext2D, node: Node): void;
