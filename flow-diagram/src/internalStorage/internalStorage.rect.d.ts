import { Node } from 'dcim-topology/core';
export declare function flowInternalStorageIconRect(node: Node): void;
export declare function flowInternalStorageTextRect(node: Node): void;
