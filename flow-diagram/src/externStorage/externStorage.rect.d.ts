import { Node } from 'dcim-topology/core';
export declare function flowExternStorageIconRect(node: Node): void;
export declare function flowExternStorageTextRect(node: Node): void;
