import { Pen, Rect } from 'dcim-topology/core';
export declare function alignNodes(pens: Pen[], rect: Rect, align: string): void;
export declare function spaceBetween(pens: Pen[], width: number): void;
