export var Lock;
(function (Lock) {
    Lock[Lock["None"] = 0] = "None"; // - 未锁定，可任意编辑。
    Lock[Lock["Readonly"] = 1] = "Readonly"; // - 只读模式，允许选中
    Lock[Lock["NoEvent"] = 2] = "NoEvent"; // - 禁止鼠标交互，无法做任何操作。纯静态画面模式。
})(Lock || (Lock = {}));
export var AnchorMode;
(function (AnchorMode) {
    AnchorMode[AnchorMode["Default"] = 0] = "Default";
    AnchorMode[AnchorMode["In"] = 1] = "In";
    AnchorMode[AnchorMode["Out"] = 2] = "Out";
})(AnchorMode || (AnchorMode = {}));
//# sourceMappingURL=status.js.map