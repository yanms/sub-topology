import { EventType } from 'dcim-topology/core/src/models';

// 轮询
var POLL = /** @class */ (function () {
    function POLL(data, message, val) {
        this.data = data;
        this.client = message;
        this.val = val;
        this.init();
    };
    POLL.prototype.init = function () {
        let topics = ''
        this.getPen(this.data.pens, topics, JSON.stringify(this.client))
    };
    POLL.prototype.getPen = function (pens, topics, message) {
        const _this = this;
        pens.forEach((pen)=>{
            if(pen.children != undefined){
            _this.getPen(pen.children, topics, message)
            }else{
                var item = pen;
                for (var _b = 0, _c = item.events; _b < _c.length; _b++) {
                    var event_1 = _c[_b];
                    // if (event_1.type === EventType.Poll) {
                    if ((event_1.type ===3 && this.val === 'Mqtt') || (event_1.type ===4 && this.val === 'Poll')
                    || (event_1.type ===5 && this.val === 'Poll1') || (event_1.type ===6 && this.val === 'Poll2')) {
                        event_1.name = topics;
                        if(message){
                            item.doSocketMqtt(event_1, message.toString());
                        }
                    }
                }
            }
        })
    };
    return POLL;
}());
export { POLL };
//# sourceMappingURL=mqtt.js.map
