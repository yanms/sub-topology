import * as mqtt from './mqtt.min.js';
import { EventType } from './models';
var MQTT = /** @class */ (function () {
    function MQTT(url, options, topics, data) {
        var _this = this;
        this.url = url;
        this.options = options;
        this.topics = topics;
        this.data = data;
        this.isEnd = false;
        this.onmessage = function (topic, message) {
            if (!_this.data.pens.length || !topic) {
                return;
            }
            if(!_this.isEnd){
                _this.getPen(_this.data.pens, topics, message)
            }
        };
        //服务器连接异常的回调
        this.onerror= function (error) {
            console.log('服务器连接异常',error)
           
        };
        this.init();
    }
    MQTT.prototype.init = function () {
        this.client = mqtt.connect(this.url, this.options);
        this.client.on('message', this.onmessage);
        if (this.topics) {
            this.client.subscribe(this.topics.split(','));
        }
    };
    MQTT.prototype.publish = function (topic, message) {
        this.client.publish(topic, message);
    };
    MQTT.prototype.close = function () {
        this.isEnd = true
        this.client.end();
    };
    MQTT.prototype.getPen = function (pens, topics, message) {
        const _this = this;
        pens.forEach((pen)=>{
            if(pen.children != undefined){
            _this.getPen(pen.children, topics, message)
            }else{
                var item = pen;
                for (var _b = 0, _c = item.events; _b < _c.length; _b++) {
                    var event_1 = _c[_b];
                    if (event_1.type === EventType.Mqtt) {
                        event_1.name = topics;
                        if(message){
                            item.doSocketMqtt(event_1, message.toString(), _this.client);
                        }
                    }
                }
            }
        })
    };
    return MQTT;
}());

export { MQTT };
//# sourceMappingURL=mqtt.js.map