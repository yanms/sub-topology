export * from './src/core';
export * from './src/options';
export * from './src/utils';
export * from './src/models';
export * from './src/middles';
//# sourceMappingURL=index.js.map
import axios from 'axios'
axios.defaults.headers.common['Authorization'] = getToken();
 /**
  * @description: 从Cookies中获取token
  * @return {token}
  */
 function getToken(){
  if(process.browser){
    var strcookie = document.cookie;//获取cookie字符串
    var arrcookie = strcookie.split("; ");//分割
    //遍历匹配
    for ( var i = 0; i < arrcookie.length; i++) {
        var arr = arrcookie[i].split("=");
        if (arr[0] == "token"){
            return arr[1];
        }
    }
  }
    return "";
}